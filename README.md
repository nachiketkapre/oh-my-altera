These are shell scripts for invoking the Intel/Altera Quartus toolchain on smallish RTL projects to quickly estimate resource utilization and timing of designs. The hardware is assumed to be the Arria10 10AX115S2F45I1SG device. (repo named after the cool .oh-my-zsh project)

----------------
How it works
----------------
The script adds all Verilog or VHDL files in given folder to a Quartus Tcl project and runs the Synthesis and Fitting phases. It then exports the area/timing/power reports for investigation by the user.

----------------
How to install
----------------

You need to clone the directory in the $HOME folder. Note that you need to rename the directory after cloning.

```git clone git@git.bitbucket.com:nachiketkapre/oh-my-altera.git ~/.oh-my-altera```

Probably want to modify PATH variable appropriately for your setup.

--------------
How to run
--------------
Set your PATH variable to include ~/.oh-my-altera. Also make sure the QUARTUS_ROOTDIR paths are properly setup as well.

```export PATH=$PATH:~/.oh-my-altera```

To compile code sitting inside a folder just run:

```alteracompile.sh <top-level-entity> <clock-name>```

To collect results in a nice summary run:
	 
```alteraresults.sh <top-level-entity>```

--------------
Testing Notes
--------------
Tested to work on Ubuntu 16.04 64b and Intel Quartus Pro 18.0 toolchain. 

--------------
Author Notes
--------------
Contact: Nachiket Kapre (nachiket@ieee.org)
