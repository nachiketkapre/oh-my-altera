# Load Quartus II Tcl Project package
package require ::quartus::project
package require ::quartus::flow
package require cmdline

proc make_all_pins_virtual {} {
	execute_module -tool map
	set name_ids [get_names -filter * -node_type pin]

	foreach_in_collection name_id $name_ids {
		set pin_name [get_name_info -info full_path $name_id]
		if { ![string equal "clk" $pin_name] } {
			post_message "Making VIRTUAL_PIN assignment to $pin_name"
			set_instance_assignment -to $pin_name -name VIRTUAL_PIN ON
		}
	}
	export_assignments
}

variable ::argv0 $::quartus(args)
set options {                   
	{ "project_name.arg" "" "Name of Project" }  
}

set usage "You need to specify options and values"
array set optshash [::cmdline::getoptions ::argv $options $usage]
puts "Project Name is $optshash(project_name)"

project_open -revision Compile $optshash(project_name)
source $optshash(project_name).tcl

# Commit assignments
make_all_pins_virtual
export_assignments

# Compile the project
execute_flow -compile

# Generate timing report_timing and resource utilization 
load_package report 
load_report

set cmd     get_fitter_resource_usage
set registers [$cmd -reg]
set les       [$cmd -le -used]
set alms      [$cmd -alm -used]
set io_pin    [$cmd -io_pin -available]
set mem_bit   [$cmd -mem_bit -percentage]
set dsp       [$cmd -resource "DSP block 9*"]
puts "Registers usage: $registers"
puts "Total used logic elements: $les"
puts "Total used ALMs: $alms"
puts "Total available I/O pins: $io_pin"
puts "Total used memory bits in percentage: ${mem_bit}%"
puts "DSP block 9-bit elements: $dsp"
# Close project
project_close                         
