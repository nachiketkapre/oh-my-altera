# Load Quartus II Tcl Project package
package require ::quartus::sta


# Generate timing report_timing and resource utilization
project_open -revision Compile Compile
# Always create the netlist first
create_timing_netlist
read_sdc clock.sdc
update_timing_netlist
# Output results in the form of messages
report_clock_fmax_summary
# Create "Fmax" report panel
report_clock_fmax_summary -panel_name "Fmax Summary"
# Report both with report panel and messages
report_clock_fmax_summary -panel_name "Fmax Summary" -stdout
# The following command is optional
delete_timing_netlist

# Close project
project_close 


