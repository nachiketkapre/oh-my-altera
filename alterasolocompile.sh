#!/bin/zsh

# --- Assumptions ---
#       Clock:          clock@3ns 
#       File:           *.vhd, *.vhdl, *.v all in same directory..
#       Results:        stored in ./results
#       Board:          DE0 - Cyclone III 

# test if we're passing argument to script..
if ((${+1}))
then
else
	echo "Usage: alterasolocompile.sh <top-level-entity-name> <clk-name>";
	exit;
fi

echo "create_clock -name $2 -period $3MHz [get_ports {$2}]" > clock.sdc
echo "derive_pll_clocks -create_base_clocks" >> clock.sdc
echo "derive_clock_uncertainty" >> clock.sdc

rm Compile.*
quartus_sh -t ~/.oh-my-altera/alterasolocompile.tcl -toplevel $1 -clk $2 &> /tmp/altera.txt | cat
quartus_sta -t ~/.oh-my-altera/alterareport.tcl &> /tmp/alteratiming.txt  | cat
