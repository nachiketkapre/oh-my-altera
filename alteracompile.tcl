# Load Quartus II Tcl Project package
package require ::quartus::project
package require ::quartus::flow
package require cmdline


proc make_all_pins_virtual { exclude} {

    puts "Excluded pin is $exclude"

    remove_all_instance_assignments -name VIRTUAL_PIN
    execute_module -tool map
    set name_ids [get_names -filter * -node_type pin]

    foreach_in_collection name_id $name_ids {
        set pin_name [get_name_info -info full_path $name_id]

        if { -1 == [lsearch -exact $exclude $pin_name] } {
            post_message "Making VIRTUAL_PIN assignment to $pin_name"
            set_instance_assignment -to $pin_name -name VIRTUAL_PIN ON
        } else {
            post_message "Skipping VIRTUAL_PIN assignment to $pin_name"
        }
    }
    export_assignments
}



variable ::argv0 $::quartus(args)
set options {
	{ "toplevel.arg" "" "Name of HDL File" }
	{ "clk.arg" "" "Name of Clock" }
}
set usage "You need to specify options and values"
array set optshash [::cmdline::getoptions ::argv $options $usage]
puts "Top-level is $optshash(toplevel)"
puts "Clock is $optshash(clk)"

exec rm -f Compile.*
if {[project_exists Compile]} {
	project_open -revision Compile Compile
} else {
	project_new -revision Compile Compile
}

# source QSF before setting devices
foreach item [glob -nocomplain *.qsf] {
	source $item
}

# Make assignments => read these from a local qsf file in the folder..
#set_global_assignment -name FAMILY "Stratix 10"
#set_global_assignment -name DEVICE 1SG280LN3F43E1VG
set_global_assignment -name FAMILY "Arria 10"
set_global_assignment -name DEVICE 10AX115S2F45I1SG

#set_global_assignment -name FAMILY "Cyclone IV E"
#set_global_assignment -name DEVICE EP4CE22F17C6
#set_global_assignment -name FAMILY "Cyclone III"
#set_global_assignment -name DEVICE EP3C16F484C6

set_global_assignment -name TOP_LEVEL_ENTITY "$optshash(toplevel)"
set_global_assignment -name SDC_FILE "clock.sdc"

foreach item [glob -nocomplain *.v] {
	set_global_assignment -name VERILOG_FILE $item
}
foreach item [glob -nocomplain *.v] {
	set_global_assignment -name SYSTEMVERILOG_FILE $item
}
foreach item [glob -nocomplain *.vv] {
	set_global_assignment -name SYSTEMVERILOG_FILE $item
}
foreach item [glob -nocomplain *.vhd] {
	set_global_assignment -name VHDL_FILE [glob -nocomplain *.vhd]
}
foreach item [glob -nocomplain *.vhdl] {
	set_global_assignment -name VHDL_FILE [glob -nocomplain *.vhdl]
}
foreach item [glob -nocomplain *.qip] {
	set_global_assignment -name QIP_FILE $item
}

# Print the top level module name
set top_level [get_global_assignment -name TOP_LEVEL_ENTITY]
puts "Top Level : $top_level"

# Print the added verilog files
set verilog_files [get_all_global_assignments -name VERILOG_FILE]
foreach_in_collection asgn $verilog_files {
	set sect_id [lindex $asgn 0]
	set name [lindex $asgn 1]		
	set value [lindex $asgn 2]
    puts "Verilog File : $value"
}

set verilog_files [get_all_global_assignments -name SYSTEMVERILOG_FILE]
foreach_in_collection asgn $verilog_files {
	set sect_id [lindex $asgn 0]
	set name [lindex $asgn 1]		
	set value [lindex $asgn 2]
    puts "System Verilog File : $value"
}

set vhdl_files [get_all_global_assignments -name VHDL_FILE]
foreach_in_collection asgn $vhdl_files {
	set sect_id [lindex $asgn 0]
	set name [lindex $asgn 1]		
	set value [lindex $asgn 2]
    puts "VHDL File : $value"
}

set qip_files [get_all_global_assignments -name QIP_FILE]
foreach_in_collection asgn $qip_files {
	set sect_id [lindex $asgn 0]
	set name [lindex $asgn 1]		
	set value [lindex $asgn 2]
    puts "QIP File : $value"
}


# Virtualie top-level pins
make_all_pins_virtual $optshash(clk)


# Commit assignments
export_assignments

# Compile the project
execute_flow -compile

# Generate timing report_timing and resource utilization
load_package report
load_report

set cmd     get_fitter_resource_usage
set registers [$cmd -reg]
set les       [$cmd -le -used]
set alms       [$cmd -alm -used]
set io_pin    [$cmd -io_pin -available]
set mem_bit   [$cmd -mem_bit -percentage]
set dsp       [$cmd -resource "DSP block 9*"]
puts "Registers usage: $registers"
puts "Total used logic elements: $les"
puts "Total used ALMs: $alms"
puts "Total available I/O pins: $io_pin"
puts "Total used memory bits in percentage: ${mem_bit}%"
puts "DSP block 9-bit elements: $dsp"

# Close project
project_close 


